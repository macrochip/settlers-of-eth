import React, { Component } from 'react'
import GameContract from '../build/contracts/Game.json'
import PlayerView from './PlayerView.js'
import getWeb3 from './utils/getWeb3'

import './css/oswald.css'
import './css/open-sans.css'
import './css/pure-min.css'
import './App.css'

class App extends Component {
  constructor(props) {
    super(props)
    this.state = {
      web3: null,
      accounts: []
    }
  }

  componentWillMount() {
    getWeb3
    .then(results => {
      this.setState({
        web3: results.web3
      })
      this.instantiateContract()
    })
    .catch(() => {
      console.log('Error finding web3.')
    })
  }

  instantiateContract() {
    const contract = require('truffle-contract')
    const game = contract(GameContract)
    game.setProvider(this.state.web3.currentProvider)
    game.defaults({
            gas: 4512388, //a little below prod
            gasPrice: 100000000000 //realistic prod
    })

    this.state.web3.eth.getAccounts((error, accounts) => {
      game.deployed().then((instance) => {
        return this.setState({ game: instance, accounts: accounts })
      })
    })
  }

  render() {
    if (!this.state.game) {
      return (
        <p>Loading...</p>
      );
    }
    return (
      <div className="App">
        <nav className="navbar pure-menu pure-menu-horizontal">
            <a href="#" className="pure-menu-heading pure-menu-link">Settlers of Eth</a>
        </nav>

        <main className="container">
          <div className="pure-g">
            <div className="pure-u-1-1">
	             <PlayerView account={this.state.accounts[0]}
                            otherAccount={this.state.accounts[1]}
                            game={this.state.game}/>
   	           <PlayerView account={this.state.accounts[1]}
                            otherAccount={this.state.accounts[0]}
                            game={this.state.game}/>
            </div>
          </div>
        </main>
      </div>
    );
  }
}

export default App
