import React, { Component } from 'react'
import getWeb3 from './utils/getWeb3'
import TradeOfferContract from '../build/contracts/TradeOffer.json'
const contract = require('truffle-contract')
const tradeOffer = contract(TradeOfferContract)
tradeOffer.defaults({
        gas: 4512388, //a little below prod
        gasPrice: 100000000000 //realistic prod
})

class IncomingTradeOffers extends Component {
  constructor(props) {
    super(props)
    this.state = {
      game: props.game
    }
  }

  componentWillMount() {
    getWeb3.then(results => {
      tradeOffer.setProvider(results.web3.currentProvider)
    });
  }

  componentDidMount() {
    this.state.game.contract.TradeOfferSent().watch((err, response) => {
      if (err) {
        return console.log("error watching event", err);
      }
      let currentOffer;
      tradeOffer.at(response.args.tradeOffer).then((newOffer) => {
        currentOffer = newOffer;
        return newOffer.to();
      }).then((tradeTo) => {
        if (tradeTo === this.props.account) {
          this.setState({ incomingOffer: currentOffer.address});
        }
      });
    });
  }

  render() {
    if (this.state.incomingOffer) {
      return (
        <div>
          <div>Trade offer incoming:</div>
          <div>{this.state.incomingOffer}</div>
        </div>
      );
    } else {
      return (
        <div>No incoming offers</div>
      );
    }
  }
}

export default IncomingTradeOffers;
