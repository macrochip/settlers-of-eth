import React, { Component } from 'react'
import SendTradeOffers from './SendTradeOffers.js'
import IncomingTradeOffers from './IncomingTradeOffers.js'

class PlayerView extends Component {
	constructor(props) {
		super(props)
    this.onOfferChange = this.onOfferChange.bind(this)
    this.onDesireChange = this.onDesireChange.bind(this)
    this.offerTrade = this.offerTrade.bind(this)
    this.drawCard = this.drawCard.bind(this);
    this.state = {
      cards: [],
      cardIndex: 0 //this exists because there is no way in truffle to obtain the whole array from the resourceBanks getter. You must choose both the address of the resource bank and which resource to interrogate at call time.
    };
	}

	render() {
    const style = {
        border: '1px solid #ccc',
        borderRadius: '1em',
        margin: '1em'
    }
    return (
    	<div style={style}>
    		<p>Account: {this.props.account}</p>
    		<p>Cards: {this.state.cards}</p>
    		<button onClick={this.drawCard.bind(this)}>
    			Draw card
    		</button>
        <br />
        Offer:
        <input onChange={this.onOfferChange.bind(this)} />
        Desire:
        <input onChange={this.onDesireChange.bind(this)} />
        <button onClick={this.offerTrade.bind(this)}>
          Offer trade
        </button>
        <SendTradeOffers account={this.props.account} game={this.props.game}/>
        <IncomingTradeOffers account={this.props.account} game={this.props.game}/>
    	</div>
    );
	}

  onOfferChange(e) {
    this.setState({offer: e.target.value})
  }

  onDesireChange(e) {
    this.setState({desire: e.target.value})
  }

  drawCard(e) {
    this.props.game.drawCard({from: this.props.account}).then((result) => {
      return this.props.game.resourceBanks.call(this.props.account, this.state.cardIndex)
    }).then((result) => {
      return this.setState((prevState, props) => {
        prevState.cards.push(result.toString())
        prevState.cardIndex += 1; //TODO: this increment should happen after the card draw, not after the resourceBanks getter
        return {prevState}
      })
    }).catch((error) => {
      console.log(error);
    })
  }

  offerTrade(e) {
    this.props.game.offerTrade(this.props.otherAccount, this.state.offer, this.state.desire, {from: this.props.account}).then((result) => {
      console.log(this.props.otherAccount, this.state.offer, this.state.desire, {from: this.props.account});
    }).catch((error) => {
      console.log(error);
    })
  }
}

export default PlayerView
