pragma solidity ^0.4.18;
contract TradeOffer {

    address public from;
    address public to;
    uint public offeredResource;
    uint public desiredResource;

    function TradeOffer(address _from, address _to, uint _offeredResource, uint _desiredResource) public {
        from = _from;
        to = _to;
        offeredResource = _offeredResource;
        desiredResource = _desiredResource;
    }
}
