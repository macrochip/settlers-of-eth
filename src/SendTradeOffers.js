import React, { Component } from 'react'

class SendTradeOffers extends Component {
  constructor(props) {
    super(props)
    this.state = {
      game: props.game
    }
  }

  componentDidMount() {
    this.state.game.tradeOffers.call(this.props.account, {from: this.props.account}).then((result) => {
      this.setState({ tradeOffers: parseInt(result, 16) === 0 ? [] : [result] });
    }).catch((error) => {
      console.log(error);
    });
  }

  render() {
    if (!this.state.tradeOffers) {
      return (
        <p>Loading trade offers sent...</p>
      );
    } else if (this.state.tradeOffers.length === 0) {
      return (
        <p>No trade offers sent</p>
      );
    }
    return (
      <div>
        <div>
          <div>Trade offer sent:</div>
          <div>{this.state.tradeOffers}</div>
        </div>
      </div>
    );
  }
}

export default SendTradeOffers
