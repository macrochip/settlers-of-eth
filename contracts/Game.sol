pragma solidity ^0.4.18;
import "./TradeOffer.sol";
contract Game {
    struct Hex {
        address owner;
        uint resourceType;
    }
    address worldOwner;
    Hex[8][8] world;
    uint constant INVALID_RESOURCE = 4;
    mapping (address => bool) players;
    mapping (address => uint[]) public resourceBanks;
    mapping (address => TradeOffer) public tradeOffers; //one trade offer per address at a time

    event TradeOfferSent(TradeOffer tradeOffer);
    event TradeOfferStart(address to, uint offeredResource, uint desiredResource, address sender);
    event CardDrawn(address person, uint reward);

    function Game() public {
        worldOwner = msg.sender;
        initResources();
    }

    modifier isLegalResource(uint resource) {
        assert(resource <= 3 && resource >= 0);
        _;
    }

    function initResources() private {
        bytes32 seed = keccak256(block.blockhash(block.number - 1)); //might not need to hash
        uint i = 0;
        uint seedMark = 0;
        for (uint x = 0; x < world.length; x++) {
            for (uint y = 0; y < world[x].length; y++) {
                byte bits = seed[seedMark];
                if (i % 2 == 0) {
                    bits &= 12; //high bitmask
                } else {
                    bits &= 3; //low bitmask
                    seedMark++;
                }
                world[x][y].resourceType = uint(bits) % 4; //resourceType 0 - 3
                i++;
            }
        }
    }

    function grabLand(uint x, uint y) public {
        //TODO: make sure x and y aren't backwards
        require(x < world.length && y < world[0].length);
        require(players[msg.sender]);
        world[x][y].owner = msg.sender;
    }

    function drawCard() public {
        players[msg.sender] = true;
        uint rewardType = insecurerandom();
        assert(rewardType <= 3 && rewardType >= 0);
        CardDrawn(msg.sender, rewardType);
        add(msg.sender, rewardType); //currently just combine cards and world map resources
    }

    //TODO can only be called once per block since there is no looping
    function insecurerandom() private view returns(uint) {
        bytes32 seed = keccak256(block.blockhash(block.number - 1)); //might not need to hash
        byte bits = seed[0];
        bits &= 3; //low bitmask
        return uint(bits) % 4; //resourceType 0 - 3
    }

    function add(address player, uint resource) private {
        assert(resource <= 3 && resource >= 0);
        resourceBanks[player].push(resource); //I am allowing this array to grow indefinitely instead of searching for spent resource slots. This is a time/space tradeoff
    }

    function remove(address player, uint resource) private returns (uint) {
        assert(resource <= 3 && resource >= 0);
        for (uint x = 0; x < resourceBanks[player].length; x++) {
            if (resource == resourceBanks[player][x]) {
                uint sentResource = resourceBanks[player][x];
                resourceBanks[player][x] = INVALID_RESOURCE; //set to sentinal value that is not a resouce because I don't want to deal with removing elements
                return sentResource;
            }
        }
        return INVALID_RESOURCE;
    }

    function hasResource(address testedPlayer, uint testedResource) public view returns (bool) {
        for (uint x = 0; x < resourceBanks[testedPlayer].length; x++) {
            if (testedResource == resourceBanks[testedPlayer][x]) {
                return true;
            }
        }
        return false;
    }

    //in this trade, the sender gets and to receives
    function offerTrade(address to, uint offeredResource, uint desiredResource) isLegalResource(offeredResource) isLegalResource(desiredResource) public {
        require(players[to]);
        require(players[msg.sender]);
        require(hasResource(msg.sender, offeredResource));
        require(hasResource(to, desiredResource));
        tradeOffers[msg.sender] = new TradeOffer(msg.sender, to, offeredResource, desiredResource);
        TradeOfferSent(tradeOffers[msg.sender]);
    }

    function acceptTrade(address offerer) public {
        require(players[msg.sender]);
        require(players[offerer]);
        require(tradeOffers[offerer] != TradeOffer(0));
        TradeOffer offer = tradeOffers[offerer];
        tradeOffers[offerer] = TradeOffer(0);
        //this trade is executed in this specific order so that the accepter cannot run out of gas and come out ahead
        //this special order may be unneccessary because out of gas reverts the whole thing
        remove(offer.to(), offer.offeredResource());
        remove(offer.from(), offer.desiredResource());
        add(offer.from(), offer.offeredResource());
        add(offer.to(), offer.desiredResource());
    }

//    function awardLongestRoad() public {

  //  }

    //function buildCity() public {

    //}

    //function buildRoad() public {

    //}
}
