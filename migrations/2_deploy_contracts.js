var Game = artifacts.require("./Game.sol");
var TradeOffer = artifacts.require("./TradeOffer.sol");

module.exports = function(deployer) {
  deployer.deploy(Game);
  deployer.deploy(TradeOffer);
};
