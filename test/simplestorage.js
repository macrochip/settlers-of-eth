var SimpleStorage = artifacts.require("./SimpleStorage.sol");
var Game = artifacts.require("./Game.sol");

contract('SimpleStorage', function(accounts) {

  it("...should store the value 89.", function() {
    return SimpleStorage.deployed().then(function(instance) {
      simpleStorageInstance = instance;

      return simpleStorageInstance.set(89, {from: accounts[0]});
    }).then(function() {
      return simpleStorageInstance.get.call();
    }).then(function(storedData) {
      assert.equal(storedData, 89, "The value 89 was not stored.");
    });
  });

});

contract('Game', function(accounts) {

  it("...should draw a card.", function() {
    return Game.deployed().then(function(instance) {
      gameInstance = instance;
      return gameInstance.drawCard({from: accounts[0]});
    }).then(function() {
      return gameInstance.cardResources.call(accounts[0], 0);
    }).then(function(storedData) {
      assert.true(storedData <= 3, "The value was greater than 3: " + storedData);
      assert.true(storedData >= 0, "The value was less than 0: " + storedData);
    });
  });
});
